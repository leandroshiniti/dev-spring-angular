package br.com.lcode.auladev.model.entity;

import br.com.lcode.auladev.configuration.Unique;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false,length = 150)
    @NotEmpty(message = "{campo.nome.obrigatorio}")
    private String nome;
    @Column(nullable = false, length = 11, unique = true)
    @NotEmpty(message = "{campo.cpf.obrigatorio}")
    @CPF(message = "{campo.cpf.invalido}")
    @Unique(message = "{campo.cpf.exist}")
    private String cpf;
    @Column(name = "data_cadastro", updatable = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("data_cadastro")
    private LocalDateTime dataCadastro;

    @PrePersist
    public void prePresist(){
        setDataCadastro(LocalDateTime.now());
    }
}
