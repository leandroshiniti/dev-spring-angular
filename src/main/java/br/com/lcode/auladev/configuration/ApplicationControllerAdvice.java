package br.com.lcode.auladev.configuration;

import br.com.lcode.auladev.model.entity.ApiErrors;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ApplicationControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handlerValidationErrors(MethodArgumentNotValidException ex){
        BindingResult bindingResult = ex.getBindingResult();
        List<String> messages = bindingResult.getAllErrors()
                .stream()
                .map(objectError -> objectError.getDefaultMessage())
                .collect(Collectors.toList());
        return new ApiErrors(messages);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity handerResponseStatusException(ResponseStatusException ex){
        String reason = ex.getReason();
        HttpStatus status = ex.getStatus();
        ApiErrors apiErrors = new ApiErrors(Arrays.asList(reason));
        return new ResponseEntity(apiErrors, status);
    }
    @ExceptionHandler({SQLIntegrityConstraintViolationException.class})
    public ResponseEntity handerResponseJdbcException(SQLIntegrityConstraintViolationException ex){
        String reason = handlerSave(ex.getMessage());
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ApiErrors apiErrors = new ApiErrors(Arrays.asList(reason));
        return new ResponseEntity(apiErrors, status);
    }

    public String handlerSave(String message){
        if(message.contains("23505-200")){
            return "Cpf ja esta na Base";
        }
        else{
            return message;
        }
    }

}
