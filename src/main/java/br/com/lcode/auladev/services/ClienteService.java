package br.com.lcode.auladev.services;

import br.com.lcode.auladev.model.entity.Cliente;
import br.com.lcode.auladev.repository.ClienteRepository;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Optional;

@Service
public class ClienteService {

    private ClienteRepository clienteRepository;

    public ClienteService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    public Cliente salvarCliente(Cliente cliente) throws Exception {
        return Optional.of(clienteRepository.save(cliente)).orElseThrow( () ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,"Erro no CPF"));
    }

    public void deleteById(int id) throws Exception {
        clienteRepository.findById(id).map( c -> {
                    clienteRepository.deleteById(id);
                    return Void.TYPE;
                }
        ).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario nao encontrado"));
    }

    public void updateById(int id, Cliente cliente) throws Exception {
            clienteRepository.findById(id).map(c -> {
                        c.setNome(cliente.getNome());
                        c.setCpf(cliente.getCpf());
                        return clienteRepository.save(c);
                    }
            ).orElseThrow(() ->
                    new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario nao encontrado"));
    }

    public Cliente findById(Integer id){
        return clienteRepository.findById(id).orElseThrow(() ->
            new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario nao encontrado"));
    }

}
