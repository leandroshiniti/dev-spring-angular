package br.com.lcode.auladev;

import br.com.lcode.auladev.model.entity.Cliente;
import br.com.lcode.auladev.repository.ClienteRepository;
import br.com.lcode.auladev.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DevSpringBootAngularApplication {

	@Bean
	public CommandLineRunner run(@Autowired ClienteService clienteRepository){
		return args -> {
			Cliente cliente = Cliente.builder().cpf("10495696870").nome("fulano").build();
			System.out.println(clienteRepository.salvarCliente(cliente));
//			clienteRepository.findByCPF("00000000020").ifPresentOrElse(System.out::println,
//					() -> System.out.println("Sem usuario"));
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(DevSpringBootAngularApplication.class, args);

	}

}
