package br.com.lcode.auladev.repository;

import br.com.lcode.auladev.model.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    @Query("select c from Cliente c where c.cpf = ?1")
    Optional<Cliente> findByCPF(String cpf);

}
