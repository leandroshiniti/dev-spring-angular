package br.com.lcode.auladev.repository;

import br.com.lcode.auladev.model.entity.Servico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServicoRepository extends JpaRepository<Servico, Integer> {

}
